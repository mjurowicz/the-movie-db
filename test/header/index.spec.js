import React from 'react';
import {shallow} from 'enzyme';
import {expect} from 'chai';

import Header from '../../src/app/header';

describe('<Header/>', () => {
  const wrapper = shallow(<Header/>);

  it('should render the header', () => {
    expect(wrapper.find('header')).to.have.length(1);
  });

  it('should render the img', () => {
    expect(wrapper.find('img')).to.have.length(1);
  });
});
