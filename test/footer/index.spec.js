import React from 'react';
import {shallow} from 'enzyme';
import {expect} from 'chai';

import Footer from '../../src/app/footer';

describe('<Footer/>', () => {
  const wrapper = shallow(<Footer/>);

  it('should render the footer', () => {
    expect(wrapper.find('footer')).to.have.length(1);
  });

  it('should render the img', () => {
    expect(wrapper.find('img')).to.have.length(1);
  });
});
