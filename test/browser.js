require('babel-register')();
require('./settings');

var jsdom = require('jsdom');

global.window = new jsdom.JSDOM().window;
global.document = window.document;
global.navigator = window.navigator;

documentRef = document;
