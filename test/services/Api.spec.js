import React from 'react';
import {expect} from 'chai';
import sinon from 'sinon';

import Api from '../../src/services/Api';

describe('Api', () => {
  it('should have the imageUrl property', () => {
    expect(Api).to.have.property('imagesUrl');
    expect(Api.imagesUrl).to.equal('http://image.tmdb.org/t/p/w185/');
  });

  it('should implement the searchMovie method', () => {
    expect(Api).to.have.property('searchMovie');
    expect(Api.searchMovie).to.be.a('function');
  });

  it('should call requestApi and return promise', () => {
    const stub = sinon.stub(Api, 'requestApi');

    expect(Api.searchMovie('star')).to.be.a('Promise');
    expect(stub.calledOnce).to.be.true;

    stub.restore();
  });

  it('should call call apiClient', () => {
    const mock = sinon.mock(Api.apiClient.search);

    mock.expects("getMovie").once();
    Api.searchMovie('star');
    expect(mock.verify()).to.be.true;

    mock.restore();
  });
});
