'use strict';

const fs = require('fs');
const path = require('path');
const dotenvFile = path.resolve(__dirname, '../.env');

function fsExistsSync(myDir) {
  try {
    fs.accessSync(myDir);
    return true;
  } catch (e) {
    return false;
  }
}

if (fsExistsSync(dotenvFile)) {
  require('dotenv').config({
    path: dotenvFile
  });
}

const { NODE_ENV } = process.env;

if (!NODE_ENV) {
  throw new Error(
    'The NODE_ENV environment variable is required but was not specified .'
  );
}

function getClientEnvironment(publicUrl) {
  const raw = Object.keys(process.env)
    .filter(key => /^APP_/i.test(key))
    .reduce(
      (env, key) => {
        env[key] = process.env[key];
        return env;
      },
      {
        NODE_ENV: process.env.NODE_ENV || 'development',
        PUBLIC_URL: process.env.PUBLIC_URL
      }
    );

  const stringified = {
    'process.env': Object.keys(raw).reduce((env, key) => {
      env[key] = JSON.stringify(raw[key]);

      return env;
    }, {})
  };

  return { raw, stringified };
}

module.exports = getClientEnvironment;
