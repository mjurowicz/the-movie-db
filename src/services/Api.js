import theMovieDb from 'themoviedb-javascript-library';

class Api {
  apiClient;
  imagesUrl;

  constructor(apiClient) {
    this.apiClient = apiClient;
    this.apiClient.common.api_key = process.env.APP_API_KEY;
    this.apiClient.common.timeout = 2000;
    this.imagesUrl = `${this.apiClient.common.images_uri}w185/`;
  }

  searchMovie(query) {
    return new Promise((resolve, reject) => {
      this.requestApi(query, resolve, reject);
    });
  }

  requestApi(query, resolve, reject) {
    try {
      this.apiClient.search.getMovie(
        { query: encodeURIComponent(query) },
        data => resolve(JSON.parse(data)),
        reason => reject(JSON.parse(reason))
      );
    } catch (e) {
      reject({ errors: e.message });
    }
  }
}

export default new Api(theMovieDb);
