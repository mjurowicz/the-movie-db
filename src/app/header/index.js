import React from 'react';

const Header = () => (
  <header className="tmdb-header">
    <h1>The Movie DB - Code Challange</h1>
    <img
      alt="The movie DB"
      width={50}
      height={50}
      src={`${process.env.PUBLIC_URL}/the-movie-db-logo.svg`}
    />
  </header>
);

export default Header;
