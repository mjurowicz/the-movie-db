import React from 'react';
import Header from './header';
import Footer from './footer';
import Content from './content';

const App = () => (
  <section>
    <Header />
    <Content />
    <Footer />
  </section>
);

export default App;
