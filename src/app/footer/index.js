import React from 'react';

const Footer = () => (
  <footer className="tmdb-footer">
    <img
      alt="The movie DB"
      width={50}
      height={50}
      src={`${process.env.PUBLIC_URL}/the-movie-db-logo.svg`}
    />
  </footer>
);

export default Footer;
