import React from 'react';
import PropTypes from 'prop-types';
import Api from '../../services/Api';

const SearchResults = ({ results = [] }) => (
  <section className="tmdb-search-results">
    {
      results.map(movie => (
        <article className="tmdb-movie-tile" key={movie.id}>
          <header className="tmdb-movie-tile__title">{movie.title}</header>
          <img
            className="tmdb-movie-tile__image"
            alt={movie.title}
            src={`${Api.imagesUrl}${movie.poster_path}`}
          />
        </article>
      ))
    }
  </section>
);

SearchResults.defaultProps = {
  results: []
};

SearchResults.propTypes = {
  results: PropTypes.array
};

export default SearchResults;
