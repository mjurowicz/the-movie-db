import React from 'react';
import PropTypes from 'prop-types';

const SearchErrors = ({ errors }) => (
  <section className="tmdb-search-errors">
    { errors.map((error, index) => <p key={index.toString()} className="error">{error}</p>) }
  </section>
);

SearchErrors.defaultProps = {
  errors: []
};

SearchErrors.propTypes = {
  errors: PropTypes.array
};

export default SearchErrors;
