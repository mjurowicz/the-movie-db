import React from 'react';
import PropTypes from 'prop-types';

const SearchForm = ({ onChange, onSubmit, value }) => (
  <form className="tmdb-search" onSubmit={onSubmit}>
    <input
      placeholder="Enter the movie title..."
      className="tmdb-search__input"
      id="tmdb-search"
      type="text"
      value={value}
      onChange={onChange}
    />
    <button className="tmdb-search__button">Search</button>
  </form>
);

SearchForm.defaultProps = {
  value: ''
};

SearchForm.propTypes = {
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  value: PropTypes.string
};

export default SearchForm;
