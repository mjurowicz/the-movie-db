import React, { PureComponent } from 'react';
import Api from '../../services/Api';
import SearchResults from './searchResult';
import SearchErrors from './searchErrors';
import SearchForm from './searchForm';

class Content extends PureComponent {
  constructor(props) {
    super(props);

    this.state = { query: '', results: [], errors: [] };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(event) {
    this.setState({ query: event.target.value });
  }

  onSubmit(event) {
    event.preventDefault();

    Api.searchMovie(this.state.query)
      .then(({ results = [] }) => { this.setState({ results, errors: [] }); })
      .catch(({ errors = [] }) => { this.setState({ errors }); });
  }

  render() {
    return (
      <div className="tmdb-content">
        <SearchForm onChange={this.onChange} onSubmit={this.onSubmit} value={this.state.query} />
        <SearchErrors errors={this.state.errors} />
        <SearchResults results={this.state.results} />
      </div>
    );
  }
}

export default Content;
