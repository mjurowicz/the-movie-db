const webpack = require('webpack');
const getClientEnvironment = require('./config/env');

const publicUrl = '';
const env = getClientEnvironment(publicUrl);

module.exports = {
  entry: [
    'react-hot-loader/patch',
    './src/index.js'
  ],
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
      {
        test: /\.scss$/,
        use: [
          "style-loader",
          "css-loader",
          "sass-loader"
        ]
      },
    ]
  },
  resolve: {
    extensions: ['*', '.js', '.jsx']
  },
  output: {
    path: __dirname + '/dist',
    publicPath: '/',
    filename: 'bundle.js'
  },
  plugins: [
    new webpack.DefinePlugin(env.stringified),
    new webpack.HotModuleReplacementPlugin()
  ],
  node: {
    fs: 'empty'
  },
  devtool: 'cheap-module-source-map',
  devServer: {
    open: true,
    contentBase: './dist',
    hot: true
  }
};
