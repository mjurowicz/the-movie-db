# The Movie DB client - code challange for Perform Group

### Build and Install

#### Prepare env variables
Copy the `.env.dist` file to `.env`.
Fill the `APP_API_KEY` property in `.env` file with The Movie DB Api key.

#### Install and run app
```
npm i
npm start
```

### Run tests

```
npm test
```
